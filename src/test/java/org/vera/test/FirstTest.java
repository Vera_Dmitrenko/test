package org.vera.test;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstTest {

    WebDriver wd;

    @Before
    public void beforeTest(){
        this.wd = new FirefoxDriver();
    }

    @Test
    public void rozetkaTest(){
        wd.get("http://rozetka.com.ua");

    }

    @After
    public void afterTest(){
        wd.close();
    }

}
